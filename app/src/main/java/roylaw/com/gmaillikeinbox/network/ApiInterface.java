package roylaw.com.gmaillikeinbox.network;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import roylaw.com.gmaillikeinbox.model.Message;

/**
 * Created by Roy on 29/3/2017.
 *
 * This class contains the rest api endpoints and the type of response it is expecting.
 * In our case we have only one endpoint i.e inbox.json
 */

public interface ApiInterface {

    @GET("inbox.json")
    Call<List<Message>> getInbox();
}
